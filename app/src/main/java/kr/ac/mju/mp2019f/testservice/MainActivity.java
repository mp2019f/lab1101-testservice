package kr.ac.mju.mp2019f.testservice;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    Button btStart;
    Button btStop;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btStart = findViewById(R.id.btStart);
        btStop = findViewById(R.id.btStop);

        btStart.setOnClickListener(this);
        btStop.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if( view == btStart ) {
            Log.d("MainActivity", "Start Button Clicked");

            //Intent i = new Intent(this, MyService.class);
            Intent i = new Intent(this, MyService.class);
            //i.putExtra("msg", "Hello Service");
            startService(i);

        } else if( view == btStop)
        {
            Log.d("MainActivity", "Stop Button Clicked");
            // Intent Service는 Stop할 수 없음
            //stopService(new Intent(this, MyService.class) );

        }

    }
}
